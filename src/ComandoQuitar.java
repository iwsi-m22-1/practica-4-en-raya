public class ComandoQuitar implements Comandos{

    ComandoPoner poner;

    public ComandoQuitar(ComandoPoner poner)   {
        this.poner = poner;
    }
    @Override
    public void execute(Tablero tablero) {
        poner.undo(tablero);
    }

    @Override
    public void undo(Tablero tablero)  {
        poner.execute(tablero);
    }

    @Override
    public void redo(Tablero tablero) {
        poner.undo(tablero);
    }

    @Override
    public String getName() {
        return null;
    }
}
