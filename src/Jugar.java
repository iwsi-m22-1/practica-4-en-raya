import java.util.*;

public class Jugar {
    private final Salida io;
    private final Tablero tablero;

    public Jugar(Salida io){
        tablero = new Tablero();
        this.io=io;
        try {
            partida(io,tablero);
        } catch (TableroLleno e) {
            io.escribir(e.getMessage());
        }
    }

    public static void main(String[] args) {
        new Jugar(new Colores());
    }

    public static int comprobarColumna(Tablero tablero,Salida io) {
        int columna = 0;
        columna = dentroTablero(io);
        try {
        if(!tablero.estaLibre(0, columna - 1)) throw  new ColumnaLlenaException();
        }catch (ColumnaLlenaException e){
            io.escribir(e.getMessage());
            while (!tablero.estaLibre(0, columna - 1)) {
                columna = dentroTablero(io);
            }
        }
        return columna;
    }

    private static int dentroTablero(Salida io) {
        int columna = 0;
        boolean valido = true;
        do {
            io.escribir("Indique la columna: ");
            columna = Consola.leerEntero(io);
            try {
                if ((columna > 7 || columna < 0)) throw new FueraDeIndicesException();
            }catch (FueraDeIndicesException e ){
                while ((columna > 7 || columna < 0)) {
                    io.escribir(e.getMessage());
                    io.escribir("Vuelva a indicar la columna");
                    columna = Consola.leerEntero(io);
                }
            }

        } while (!valido);

        return columna;
    }

    private static void partida(Salida io,Tablero tablero) throws TableroLleno{

        Victoria victoria = new Victoria(tablero, 4);
        ArrayList<ModosJuego> modosJuego= new ArrayList<>();
        modosJuego.add(new Basico(tablero, victoria,io));
        modosJuego.add(new Entrenamiento(tablero,victoria,io));
        modosJuego.add(new Demo(tablero, victoria,io));
        boolean ganar = true;
        int eleccion, continuar = 0;
        do {
            tablero.inicializar();
            eleccion = io.menu(io);
            switch (eleccion) {
                case 1:
                    ganar = modosJuego.get(0).jugar();
                    break;

                case 2:
                    ganar = modosJuego.get(1).jugar() ;
                    break;

                case 3:
                    ganar =modosJuego.get(2).jugar();
                    break;
            }
            if (!ganar) throw new TableroLleno();
            if (eleccion != 0) {
                continuar = io.continuar(io);
            }

        } while (eleccion != 0 && continuar == 1);
    }

}
