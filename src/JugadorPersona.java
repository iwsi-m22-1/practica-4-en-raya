
public class JugadorPersona extends Jugador {


    public JugadorPersona(Ficha ficha,Salida io) {
        super(ficha,io);
    }

    public void poner(Tablero tablero){
        int columna= Jugar.comprobarColumna(tablero,io);
        tablero.insertarFicha(columna-1,super.ficha);
    }

    @Override
    public void quitar(Tablero tablero) {
        super.quitar(tablero);
    }
}
