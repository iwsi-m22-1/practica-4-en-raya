public abstract class Jugador {
    Ficha ficha;
    Salida io;

    public Jugador(Ficha ficha,Salida io) {
        this.ficha = ficha;
        this.io=io;
    }

    abstract void poner(Tablero tablero);

    public void quitar(Tablero tablero){
        tablero.quitarFicha();
    }


}
