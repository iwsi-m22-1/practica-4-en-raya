public class   Turno {
    private static int turno = 0;

    public static int quienJuega(){
        return turno;
    }

    public static void cambiar(){
        turno=(turno+1)%2;
    }

    public static int quienNoJuega(){
        return (turno+1)%2;

    }

    public static void reiniciar(){
        turno=0;
    }
}
