
public abstract class ModosJuego {
    Tablero tablero;
    Victoria victoria;
    Salida io;

    public ModosJuego(Tablero tablero, Victoria victoria,Salida io) {
        this.tablero = tablero;
        this.victoria = victoria;
        this.io=io;
    }

    abstract boolean jugar();

    public void gana(Jugador jugador,Salida io){
        io.escribir("Has ganado jugador "+jugador.ficha.getColor());
    }
}
