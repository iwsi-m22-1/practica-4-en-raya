import java.util.*;

public class GestorComandos {
    private static final GestorComandos gestor = null;
    private final Stack<Comandos> pilaundo;
    private final Stack<Comandos> pilaredo;
    private final List<String> historiadecomandos;

    static GestorComandos getInstance() {
        return Objects.requireNonNullElseGet(gestor, GestorComandos::new);
    }
    private GestorComandos () {
        pilaredo = new Stack<>();
        pilaundo = new Stack<>();
        historiadecomandos = new ArrayList<>();
    }


    public void execute(Comandos comando,Tablero tablero) {
        comando.execute(tablero);
        pilaundo.push(comando);
        historiadecomandos.add(comando.getName());
    }

    public void undo(Tablero tablero) {
        if(!pilaundo.empty()){
            Comandos comando= pilaundo.pop();
            comando.undo(tablero);
            pilaredo.push(comando);
            historiadecomandos.add(comando.getName());
        }
    }

    public void redo(Tablero tablero) {
        if(!pilaredo.empty()){
            Comandos comando= pilaredo.pop();
            comando.execute(tablero);
            pilaundo.push(comando);
            historiadecomandos.add(comando.getName());
        }
    }

    public void vaciarUndo() {
        pilaundo.removeAllElements();
    }

    public void vaciarRedo() {
        pilaredo.removeAllElements();
    }

    public List<String> getHistoriaComandos() {
        return historiadecomandos;
    }


}
