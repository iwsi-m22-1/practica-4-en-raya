import java.util.InputMismatchException;
import java.util.Scanner;

public class Consola {

        private static Scanner s = null;

        private Consola() {
            s = new Scanner(System.in).useDelimiter("\n");
        }

        public static int leerEntero(Salida io) {
            boolean seguir;
            int opcion=0;
            do {
                try {
                    if (s == null) {
                        new Consola();
                        opcion=s.nextInt();
                    }else{
                        opcion=s.nextInt();
                    }
                    seguir=false;
                } catch (InputMismatchException e) {
                    io.escribir("Introduzca un valor numerico dentro del rango: ");
                    LimpiarConsola();
                    seguir=true;

                }
            }while (seguir);
            return opcion;
        }

        public static void LimpiarConsola(){
            if(s==null){
                new Consola();
            }else{
                s.nextLine();
            }
        }

}
