public interface Salida {

    void escribir(String mensaje);
    void dibujarTablero(Tablero tablero);
    int menu(Salida io);
    int continuar(Salida io);

}
