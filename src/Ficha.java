public class Ficha {
    private char letra;
    private String color;


    public Ficha(String color) {
        if(color.equalsIgnoreCase("AMARILLO")){
            this.letra = 'A';
            this.color = color;
        } else if (color.equalsIgnoreCase("ROJO")){
            this.letra = 'R';
            this.color = color;
        }
    }

    public String getColor() {
        return color;
    }


    public String dibujar() {
        return " " + Character.toString(letra) + " ";
    }

}

