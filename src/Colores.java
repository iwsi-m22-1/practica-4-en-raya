public class Colores extends Escribir {

    public static final String RESET  = "\u001B[0m";
    public static final String NARANJA = "\u001B[34m";
    public static final String VERDE  = "\u001B[32m";
    public static final String MORADO = "\u001B[35m";
    public static final String AZUL   = "\u001B[36m";

    public void escribir(String s){
        System.out.print(MORADO);
        super.escribir(s);
        System.out.print(RESET);
    }

    public void dibujarTablero(Tablero tablero) {
        System.out.print(AZUL);
        super.dibujarTablero(tablero);
        System.out.print(RESET);
    }

    public int menu(Salida io) {
        System.out.print(VERDE);
        return super.menu(io);
    }

    public int continuar(Salida io) {
        System.out.println(NARANJA);
        return super.continuar(io);
    }
}
