public interface   Comandos {

    void execute(Tablero tablero);
    void undo(Tablero tablero) ;
    void redo(Tablero tablero) ;
    String getName();
}
