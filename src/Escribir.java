
public class Escribir implements Salida{

    public void  dibujarTablero(Tablero t){
        System.out.println("  1   2   3   4   5   6   7 ");
        System.out.println("-----------------------------");
        for (int i = 0; i < t.getFILA(); i++) {
            for (int j = 0; j < t.getCOLUMNA(); j++) {
                System.out.print("|");
                dibujarCasilla(t.tablero[i][j]);
            }
            System.out.println("|");
        }
        System.out.println("-----------------------------");
    }

    private static void dibujarCasilla(Casilla c){
        if(c.isVacia()){
            System.out.print(c.getCASILLAVACIA());
        }else {
            System.out.print(dibujarColor(c.getFicha()));
            System.out.print("\u001B[36m");
        }
    }

    public void escribir(String s){
        System.out.println(s);
    }

    public static String dibujarColor(Ficha ficha){
        if(ficha.getColor().equals("ROJO")){
            System.out.print("\u001B[31m");
            return ficha.dibujar();

        }else {
            System.out.print("\u001B[33m");
            return ficha.dibujar();
        }
    }

    public int menu(Salida io){
        int eleccion;
        System.out.println("¿A que modo de juego desea jugar?");
        System.out.println("    1-Básico");
        System.out.println("    2-Entrenamiento");
        System.out.println("    3-Demo");
        System.out.println("    0-Salir");
        System.out.print("Escriba su elección: ");
        eleccion= Consola.leerEntero(io);
        if (eleccion>3 || eleccion<0){
            System.out.println();
            System.out.println("^^Opción no válida^^");
        }
        return eleccion;
    }

    public int continuar(Salida io){
        int continuar;
        do {
            System.out.print("Juego terminado, ¿desea continuar?(Y:1/N:0): ");
            continuar = Consola.leerEntero(io);
            if (continuar > 1) {
                System.out.println("Opción incorrecta");
            }
        }while (continuar > 1);
        return continuar;
    }

}
