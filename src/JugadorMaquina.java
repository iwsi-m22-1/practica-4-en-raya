public class   JugadorMaquina extends  Jugador{

    public JugadorMaquina(Ficha ficha,Salida io) {
        super(ficha,io);
    }

    @Override
    public void poner(Tablero tablero) {
        try {
            tablero.insertarFicha(numAleatorio(tablero) - 1, super.ficha);
            Thread.sleep(2 * 1000);
        } catch (Exception e) {
            io.escribir(e.getMessage());
        }
    }
    private int numAleatorio(Tablero tablero){
        int min=0,max=tablero.getCOLUMNA();
        boolean esCorrecta=false;
        int columna=0;
        while (!esCorrecta){
             columna = (int)(Math.random()*((max-min)+1))+min;
            if(columna > 7 || columna < 1) {
                columna = (int)(Math.random()*((max-min)+1))+min;
            }else{
                if(tablero.estaLibre(0, columna - 1)){
                    esCorrecta=true;
                }
            }
        }
        return columna;
    }

}
