public class   Casilla {
    private final String CASILLAVACIA="   ";
    private int fila=0;
    private int columna=0;
    private Ficha ficha;

    public Casilla(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }

    public Casilla(int fila, int columna, Ficha ficha) {
        this.fila = fila;
        this.columna = columna;
        this.ficha = ficha;
    }

    public boolean isVacia(){
        return ficha==null;
    }

    public void setFicha(Ficha ficha) {
        this.ficha = ficha;
    }

    public Ficha getFicha() {
        return ficha;
    }

    public String getCASILLAVACIA() {
        return CASILLAVACIA;
    }
}
