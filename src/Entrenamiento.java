

public class Entrenamiento extends  ModosJuego{

    Jugador [] jugadores;
    GestorComandos gestor = GestorComandos.getInstance();
    private static final int maquina=0;
    private static final int persona=1;

    public Entrenamiento(Tablero tablero, Victoria victoria,Salida io) {
        super(tablero, victoria,io);
        jugadores=new Jugador[2];
        jugadores[persona]=new JugadorPersona(new Ficha("ROJO"),io);
        jugadores[maquina]= new JugadorMaquina(new Ficha("Amarillo"),io);
    }

    public boolean jugar() {
        boolean ganarPersona=false;
        boolean ganarMaquina=false;
        io.dibujarTablero(tablero);
        while (!tablero.estaLleno() && !ganarPersona && !ganarMaquina) {
            ganarPersona=jugador(ganarPersona,jugadores[persona],io);

            if (!ganarPersona) {
                io.escribir(jugadores[maquina].ficha.getColor());
                jugadores[maquina].poner(tablero);
                ganarMaquina = victoria.hayGanador();
                io.dibujarTablero(tablero);
            }
        }

        if(ganarPersona){
            super.gana(jugadores[persona],io);
            return ganarPersona;
        }else{
            super.gana(jugadores[maquina],io);
            return ganarMaquina;
        }
    }

    private boolean jugador(boolean ganar,Jugador jugador,Salida io){
        ComandoPoner poner= new ComandoPoner(jugador);
        boolean ganar1=false;
        int opccion=0;
        do{
            if (!ganar) {
                io.escribir(jugadores[persona].ficha.getColor());
                gestor.execute(poner,tablero);

                io.dibujarTablero(tablero);

                opccion= quitarFicha(io);

                if(opccion==2){
                    ganar1 = victoria.hayGanador();
                }
            }
        }while (opccion==1);

        return ganar1;
    }

    private int quitarFicha(Salida io){
        io.escribir("Desea realizar un cambio de posicion:");
        io.escribir("[1] SI");
        io.escribir("[2] NO");
        io.escribir("Opcion a elegir: ");
        int opcion;

        do {
            opcion = Consola.leerEntero(io);
            try{
                if (opcion < 1 || opcion > 2) throw new FueraDeIndicesException();
            }catch (FueraDeIndicesException e){
                io.escribir(e.getMessage());
            }
            if (opcion == 1) {
                gestor.undo(tablero);
            }
        } while (opcion < 1 || opcion > 2);

        return opcion;
    }


}
