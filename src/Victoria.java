public class Victoria {

     private Tablero t;
     private final int numFichasVictoria;

    public Victoria(Tablero t, int numFichasVictoria) {
        this.t = t;
        this.numFichasVictoria = numFichasVictoria;
    }

    public boolean hayGanador() {
        boolean ganar=false;
        if(t.getNumfichasTablero() > 6){
            ganar=(ganarFila() || ganarColumna() || ganarDiagonal());
        }
        return ganar;
    }

    private boolean ganarFila() {

        int numFichasIguales = 1;//empieza en uno por la ficha que tomas como referencia
        int columnaDerecha = t.getColumna(), columnaIzquierda = t.getColumna();


        boolean encontradoIzquierda = true;//buscar hacia la izquierda de la ficha
        while ((columnaIzquierda > 0 && columnaIzquierda > t.getColumna() - (numFichasVictoria-1)) && encontradoIzquierda) {
            if (t.tablero[t.getFila()][columnaIzquierda - 1].getFicha() != null) {
                if (t.tablero[t.getFila()][columnaIzquierda - 1].getFicha().getColor().equals(t.tablero[t.getFila()][t.getColumna()].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontradoIzquierda = false;
                }
                columnaIzquierda--;
            } else {
                encontradoIzquierda = false;
            }
        }

        //buscar hacia la derecha de la ficha
        boolean encontradoDerecha = true;
        while ((columnaDerecha < t.getCOLUMNA() - 1 && columnaDerecha < t.getColumna() + (numFichasVictoria-1)) && encontradoDerecha && numFichasIguales < numFichasVictoria) {
            if (t.tablero[t.getFila()][columnaDerecha + 1].getFicha() != null) {
                if (t.tablero[t.getFila()][t.getColumna()].getFicha().getColor().equals(t.tablero[t.getFila()][columnaDerecha + 1].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontradoDerecha = false;
                }
                columnaDerecha++;
            } else {
                encontradoDerecha = false;
            }
        }
        return numFichasIguales >= numFichasVictoria;
    }

    private boolean ganarColumna() {
        //buscas si hay 4 fichas iguales hacia abajo
        int numFichasIguales = 1;//empieza en uno por la ficha que tomas como referencia
        int fila= t.getFila();
        boolean encontrado = true;
        while ((fila < t.getFILA() - 1) && encontrado && numFichasIguales<=numFichasVictoria) {
            if (t.tablero[t.getFila()][t.getColumna()].getFicha().getColor().equals(t.tablero[fila + 1][t.getColumna()].getFicha().getColor())) {
                numFichasIguales++;
            } else {
                encontrado = false;
            }
            fila++;
        }
        return numFichasIguales >=numFichasVictoria;
    }

    private boolean ganarDiagonal() {

        boolean encontrado = false;
        int diagonalPrincipal, diagonalSecundaria ;
        //suma la cantidad de fichas diagonal de izquierda a derecha
        diagonalPrincipal= diagonalPrincipalAr()+diagonalPrincipalAb();
        //suma la cantidad de fichas diagonal de derecha a izquierda
        diagonalSecundaria= diagonalSecunadariaAr()+diagonalSecundariaAb();

        if (diagonalPrincipal >= 4 || diagonalSecundaria >= 4) {
            encontrado = true;
        }

        return encontrado;
    }

    private int diagonalPrincipalAr() {

        boolean encontrado = true;
        int columna = t.getColumna(), fila = t.getFila(), numFichasIguales = 1;

        while (encontrado && columna > 0 && columna > t.getColumna() - (numFichasVictoria-1) && fila > 0 && fila > t.getFila() -(numFichasVictoria-1)) {//comprobar que no hay más de tres en esa diagonal
            if (t.tablero[fila - 1][columna - 1].getFicha() != null) {
                if (t.tablero[fila - 1][columna - 1].getFicha().getColor().equals(t.tablero[t.getFila()][t.getColumna()].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontrado = false;
                }
                columna--;
                fila--;
                //desplazar a la casilla de arriba a la izquierda
            } else {
                encontrado = false;
            }
        }
        return numFichasIguales;
    }

    private int diagonalPrincipalAb() {

        boolean encontrado = true;
        int columna = t.getColumna(), fila = t.getFila(), numFichasIguales = 0;

        while (encontrado && columna < t.getCOLUMNA()-1 && columna < t.getColumna() + (numFichasVictoria-1) && fila < t.getFILA()- 1 && fila < t.getFila() + (numFichasVictoria-1)) {//comprobar que no hay más de tres en esa diagonal
            if (t.tablero[fila + 1][columna +1].getFicha() != null) {
                if (t.tablero[fila + 1][columna + 1].getFicha().getColor().equals(t.tablero[t.getFila()][t.getColumna()].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontrado = false;
                }
                columna++;
                fila++;
                //desplazar a la casilla de abajo a la derecha
            } else {
                encontrado = false;
            }
        }
        return numFichasIguales;
    }

    private int diagonalSecunadariaAr() {
        boolean encontrado = true;
        int columna = t.getColumna(), fila = t.getFila(), numFichasIguales = 1;

        while ( encontrado && columna < t.getCOLUMNA()-1 && columna < t.getColumna() + (numFichasVictoria-1)  && fila > 0 && fila > t.getFila() - (numFichasVictoria-1)) {//comprobar que no hay más de tres en esa diagonal
            if (t.tablero[fila - 1][columna + 1].getFicha() != null) {
                if (t.tablero[fila - 1][columna + 1].getFicha().getColor().equals(t.tablero[t.getFila()][t.getColumna()].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontrado = false;
                }
                columna++;
                fila--;
                //desplazar a la casilla arriba a la derecha
            } else {
                encontrado = false;
            }
        }
        return numFichasIguales;
    }

    private int diagonalSecundariaAb() {
        boolean encontrado = true;
        int columna = t.getColumna(), fila = t.getFila(), numFichasIguales = 0;

        while (encontrado && columna > 0 && columna > t.getColumna() - (numFichasVictoria-1) && fila < t.getFILA() - 1 && fila < t.getFila() + (numFichasVictoria-1)) {//comprobar que no hay más de tres en esa diagonal
            if (t.tablero[fila + 1][columna - 1].getFicha() != null) {
                if (t.tablero[fila + 1][columna - 1].getFicha().getColor().equals(t.tablero[t.getFila()][t.getColumna()].getFicha().getColor())) {
                    numFichasIguales++;
                } else {
                    encontrado = false;
                }
                columna--;
                fila++;
                //desplazar a la casilla abajo a la izquierda
            } else {
                encontrado = false;
            }
        }
        return numFichasIguales;
    }
}
