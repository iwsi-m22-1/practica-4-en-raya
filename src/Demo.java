public class Demo extends ModosJuego {
    JugadorMaquina[] jugadores;

    public Demo(Tablero tablero, Victoria victoria,Salida io) {
        super(tablero, victoria,io);
        jugadores = new JugadorMaquina[2];
        jugadores[0] = new JugadorMaquina(new Ficha("ROJO"),io);
        jugadores[1] = new JugadorMaquina(new Ficha("Amarillo"),io);
    }

    public boolean jugar() {
        boolean ganar = false;

        io.dibujarTablero(tablero);
        while (!tablero.estaLleno() && !ganar) {
            io.escribir(jugadores[Turno.quienJuega()].ficha.getColor());
            jugadores[Turno.quienJuega()].poner(tablero);
            ganar = victoria.hayGanador();
            io.dibujarTablero(tablero);
            Turno.cambiar();
            if (!ganar) {
                io.escribir(jugadores[Turno.quienJuega()].ficha.getColor());
                jugadores[Turno.quienJuega()].poner(tablero);
                ganar = victoria.hayGanador();
                io.dibujarTablero(tablero);
                Turno.cambiar();
            }
        }

        if(ganar){
            super.gana(jugadores[Turno.quienNoJuega()],io);
        }

        Turno.reiniciar();
        return ganar;
    }

}
