

public class Basico extends ModosJuego {
    JugadorPersona[] jugadores;
    GestorComandos gestor = GestorComandos.getInstance();

    public Basico(Tablero tablero, Victoria victoria,Salida io) {
        super(tablero, victoria,io);
        jugadores = new JugadorPersona[2];
        jugadores[0] = new JugadorPersona(new Ficha("ROJO"),io);
        jugadores[1] = new JugadorPersona(new Ficha("Amarillo"),io);
    }

    public boolean jugar() {
        boolean ganar = false;
        while (!tablero.estaLleno() && !ganar) {

            ganar = jugador(ganar, jugadores[Turno.quienJuega()],io);
        }
        if (ganar) {
            super.gana(jugadores[Turno.quienNoJuega()],io);
        }

        Turno.reiniciar();

        return ganar;
    }

    private boolean jugador(boolean ganar, JugadorPersona jugador,Salida io) {
        ComandoPoner poner = new ComandoPoner(jugador);
        boolean ganar1 = false;
        io.dibujarTablero(tablero);
        int opcion = 0;
        do {
            if (!ganar) {
                io.escribir(jugadores[Turno.quienJuega()].ficha.getColor());
                gestor.execute(poner, tablero);

                io.dibujarTablero(tablero);

                opcion = quitarFicha(io);

                if (opcion == 2) {
                    ganar1 = victoria.hayGanador();
                }
            }
        } while (opcion == 1);

        Turno.cambiar();

        return ganar1;
    }

    private int quitarFicha(Salida io) {
        io.escribir("Desea realizar un cambio de posicion:");
        io.escribir("[1] SI");
        io.escribir("[2] NO");
        io.escribir("Opcion a elegir: ");
        int opcion;

        do {
            opcion = Consola.leerEntero(io);
            try{
                if (opcion < 1 || opcion > 2) throw new FueraDeIndicesException();
            }catch (FueraDeIndicesException e){
                io.escribir(e.getMessage());
            }
            if (opcion == 1) {
                gestor.undo(tablero);
            }
        } while (opcion < 1 || opcion > 2);

        return opcion;
    }


}