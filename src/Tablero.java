public class Tablero {
    final Casilla[][] tablero;
    private final int FILA = 6;
    private final int COLUMNA = 7;
    private int numfichasTablero = 0;
    private int fila = 0, columna = 0; //guarda la posicion de la ultima ficha introducida

    public Tablero() {
        this.tablero = new Casilla[FILA][COLUMNA];
    }

    public int getFILA() {
        return FILA;
    }

    public int getCOLUMNA() {
        return COLUMNA;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public int getNumfichasTablero() {
        return numfichasTablero;
    }

    public void inicializar() {
        for (int i = 0; i < FILA; i++) {
            for (int j = 0; j < COLUMNA; j++) {
                tablero[i][j] = new Casilla(i, j, null);
            }
        }
    }

    public boolean   estaLibre(int fila, int columna) {
        return tablero[fila][columna].isVacia();
    }

    public void insertarFicha(int columna, Ficha ficha) {
        int i = 0;
        while (i < FILA - 1 && estaLibre(i, columna)) {
            i++;
        }
        if (i < FILA && !estaLibre(i, columna)) {
            tablero[i - 1][columna].setFicha(ficha);
            numfichasTablero++;
            fila = i - 1;
            this.columna = columna;
        } else {
            tablero[i][columna].setFicha(ficha);
            numfichasTablero++;
            fila = i;
            this.columna = columna;
        }
    }

    public void quitarFicha(){
        tablero[fila][columna].setFicha(null);
    }


    public boolean estaLleno() {
        boolean lleno = false;
        int j = 0;
        for (int i = 0; i < COLUMNA; i++) {
            if (!estaLibre(0, i)) {
                j++;
            }
        }
        if (j == COLUMNA) {
            lleno = true;
        }
        return lleno;
    }


}
