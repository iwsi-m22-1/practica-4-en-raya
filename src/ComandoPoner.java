
public class ComandoPoner implements Comandos{

    Jugador persona;

    public ComandoPoner(Jugador persona) {
        this.persona = persona;
    }

    @Override
    public void execute(Tablero tablero)  {
        persona.poner(tablero);
    }

    @Override
    public void undo(Tablero tablero) {
        persona.quitar(tablero);
    }

    @Override
    public void redo(Tablero tablero)    {
        persona.poner(tablero);
    }

    @Override
    public String getName() {
        return "Poner ficha";
    }
}
